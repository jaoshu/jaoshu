// JavaScript Document
//https://stackoverflow.com/questions/19351408/creating-an-array-of-image-objects

var createImage = function (src) {
    var img = new Image();
    img.src = src;
    return img;
}

//And one for the quote array.
var createQuote = function (quote) {
    var string = new String();
    quote.string = this.string;
    return quote;
}

var imageArray = ["Lincoln.png", "JS.jpeg", "Madison.jpeg", "hamilton.jpeg","bonhoeffer.jpg","HelmuthHubener.jpeg"];

var quoteArray = ["America will never be destroyed from the outside. If we falter and lose our freedoms, it will be because we destroyed ourselves.\
--Abraham Lincoln",
 "By free government, we do not mean that a man has a right to steal, rob, etc. but free from bondage, taxation,\
 and oppression–free in every thing if he conduct himself honestly and circumspectly with his neighbor—free in a Spiritual capacity.--Joseph Smith",
"Oppressors can tyrannize only when they achieve a standing army, an enslaved press, and a disarmed populace.--James Madison",
"A nation which can prefer disgrace to danger is prepared for a master, and deserves one.--Alexander Hamilton",
"It is so easy to overestimate the importance of our own achievements compared with what we owe to the help of others.--Dietrich Bonhoeffer",
"Through their unscrupulous terror tactics against young and old, men and women,\
they have succeeded in making you spineless puppets to do their bidding.--Helmuth Hubener"];