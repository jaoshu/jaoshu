"use strict";


/*****************************************************************************
* Separating quotes from images for home page.
* Tackle quotes here.
******************************************************************************/

var quoteArray = ["\"America will never be destroyed from the outside. If we \
falter and lose our freedoms, it will be because we destroyed ourselves.\"\
\r --Abraham Lincoln",
 "\"By free government, we do not mean that a man has a right to steal, rob, etc.\
  but free from bondage, taxation, and oppression–free in every thing if he \
conduct himself honestly and circumspectly with his neighbor—free in a \
 spiritual capacity.\"        \r  --Joseph Smith",
"\"Oppressors can tyrannize only when they achieve a standing army, an enslaved\
press, and a disarmed populace.\"         --James Madison",
"\"A nation which can prefer disgrace to danger is prepared for a master, and \
deserves one.\"\r   --Alexander Hamilton",
"\"Silence in the face of evil is evil itself.\"\r  --Dietrich Bonhoeffer",
"\"Through their unscrupulous terror tactics against young and old, men and women,\
 they have succeeded in making you spineless puppets to do their bidding.\"\
\r--Helmuth Hubener",
"\"A primary object should be the education of our youth \
in the science of government. In a republic, what\
species of knowledge can be equally important? And what\
duty more pressing than communicating it to those who\
are to be the future guardians of the liberties of the\
country?\"\r   --George Washington"];

/*****************************************************************************
*Quotes display function.
*****************************************************************************/
var position = 0;
    setInterval(function()
    {
      if(position === quoteArray.length)
      {
        position = 0;
      }
      document.getElementById("quoteID").textContent = quoteArray[position];
      position++;
    },18000);