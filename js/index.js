"use strict";

/*****************************************************************************
*Instantiated two arrays: images and quotes.
*****************************************************************************

var imageArray = ["Lincoln.png",
                  "Joseph.jpeg",
                  "Madison.jpeg",
                  "hamilton.jpeg",
                  "bonhoeffer.jpeg",
                  "HelmuthHubener.jpeg",
                  "Washington.jpeg"];*/

var quoteArray = ["\"America will never be destroyed from the outside. If we \
falter and lose our freedoms, it will be because we destroyed ourselves.\"\
\n --Abraham Lincoln",
 "\"By free government, we do not mean that a man has a right to steal, rob, etc.\
  but free from bondage, taxation, and oppression–free in every thing if he \
conduct himself honestly and circumspectly with his neighbor—free in a \
 Spiritual capacity.\"        \n  --Joseph Smith",
"\"Oppressors can tyrannize only when they achieve a standing army, an enslaved\
press, and a disarmed populace.\"         --James Madison",
"\"A nation which can prefer disgrace to danger is prepared for a master, and \
deserves one.\"\n   --Alexander Hamilton",
"\"Silence in the face of evil is evil itself.\"\n  --Dietrich Bonhoeffer",
"\"Through their unscrupulous terror tactics against young and old, men and women,\
 they have succeeded in making you spineless puppets to do their bidding.\"\
\n--Helmuth Hubener",
"\"A primary object should be the education of our youth \
in the science of government. In a republic, what\
species of knowledge can be equally important? And what\
duty more pressing than communicating it to those who\
are to be the future guardians of the liberties of the\
country?\"\\n   --George Washington"];

/*****************************************************************************
*Images display function.
*https://stackoverflow.com/questions/42003566/how-to-automatically-change-an-html
*-image-using-javascript
*****************************************************************************
var index = 0;
setInterval (function(){
  if (index === imageArray.length) {
    index = 0;
  }
  document.getElementById("switchImg").src = imageArray[index];
  index++;
} , 18000);*/

/*****************************************************************************
*Quotes display function.
*****************************************************************************/
var position = 0;
    setInterval(function(){
      if(position === quoteArray.length){
        position = 0;
      }
      document.getElementById("quoteID").textContent = quoteArray[position];
      position++;
    },18000);

//The following sources contain the keys needed to "synchronize" these arrays.
//https://stackoverflow.com/questions/36308187/how-to-assign-multiple-values-in-a-single-variable-in-javascript
//https://stackoverflow.com/questions/3984735/js-nested-arrays#3984773
